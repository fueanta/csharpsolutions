/*
Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.

Note:
-----
1. The number of elements initialized in nums1 and nums2 are m and n respectively.
2. You may assume that nums1 has enough space (size that is greater or equal to m + n)
   to hold additional elements from nums2.

Examples:
---------
Input:
nums1 = [1,2,3,0,0,0], m = 3
nums2 = [2,5,6],       n = 3

Output: [1,2,2,3,5,6]
----------------------------
Input:
nums1 = [1,2,3,0,0,0,0,0,0], m = 3
nums2 = [2,5,6,7,8,9],       n = 6

Output: [1,2,2,3,5,6]
----------------------------
Input:
nums1 = [0], m = 0
nums2 = [1], n = 1

Output: [1]
*/

namespace Problems.LeetCode.Array.MergeSortedArray_88
{
    public class Solution
    {
        /// <summary>
        ///     Merges two sorted arrays, one into another, in ascending order.
        ///     Uses three pointers on two arrays.
        ///     Time complexity : O(n)
        ///     Space complexity : O(1)
        /// </summary>
        /// <param name="nums1">An array of integers.</param>
        /// <param name="m">Number of non zero elements in nums1.</param>
        /// <param name="nums2">An array of integers.</param>
        /// <param name="n">Number of elements in nums2.</param>
        public void Merge(int[] nums1, int m, int[] nums2, int n)
        {
            if (n == 0)
                return;

            int ptr1 = m - 1, ptr2 = n - 1, ptr3 = m + n - 1;

            while (ptr1 > -1 && ptr2 > -1)
            {
                nums1[ptr3--] = (nums1[ptr1] > nums2[ptr2]) ? nums1[ptr1--] : nums2[ptr2--];
            }

            while (ptr1 > -1)
            {
                nums1[ptr3--] = nums1[ptr1--];
            }

            while (ptr2 > -1)
            {
                nums1[ptr3--] = nums2[ptr2--];
            }
        }

        /// <summary>
        ///     Merges two sorted arrays, one into another, in ascending order.
        ///     Uses sorting.
        ///     Time complexity : O(nlgn)
        ///     Space complexity : O(1)
        /// </summary>
        /// <param name="nums1">An array of integers.</param>
        /// <param name="m">Number of non zero elements in nums1.</param>
        /// <param name="nums2">An array of integers.</param>
        /// <param name="n">Number of elements in nums2..</param>
        public void Merge2(int[] nums1, int m, int[] nums2, int n)
        {
            for (var i = m + n - 1; i > m - 1; i--)
                nums1[i] = nums2[m + n - (i + 1)];
            System.Array.Sort(nums1);
        }
    }
}
