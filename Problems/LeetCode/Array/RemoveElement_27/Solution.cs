/*
Given an array nums and a value val, remove all instances of that value in-place and
return the new length. Do not allocate extra space for another array, you must do this by 
modifying the input array in-place with O(1) extra memory. The order of elements can be
changed. It doesn't matter what you leave beyond the new length.

Examples:
---------
Given nums = [3,2,2,3], val = 3,

Your function should return length = 2, with the first two elements of nums being 2.

It doesn't matter what you leave beyond the returned length.
-----------------------------------
Given nums = [0,1,2,2,3,0,4,2], val = 2,

Your function should return length = 5, with the first five elements of nums containing 0, 1, 3, 0, and 4.

Note that the order of those five elements can be arbitrary.

It doesn't matter what values are set beyond the returned length.

Clarification:
--------------
Confused why the returned value is an integer but your answer is an array?

Note that the input array is passed in by reference, which means modification to the input
array will be known to the caller as well. Internally you can think of this:Confused why the
returned value is an integer but your answer is an array?

Note that the input array is passed in by reference, which means modification to the input
array will be known to the caller as well. Internally you can think of this:

// nums is passed in by reference. (i.e., without making a copy)
int len = removeElement(nums, val);

// any modification to nums in your function would be known by the caller.
// using the length returned by your function, it prints the first len elements.
for (int i = 0; i < len; i++) {
    print(nums[i]);
}
*/

namespace Problems.LeetCode.Array.RemoveElement_27
{
    public class Solution
    {
        /// <summary>
        ///     Removes all the instances of a given value from an array.
        ///     Uses two pointers. Does not maintain order.
        ///     Two pointers can treverse upto n times.
        ///     Time complexity : O(n)
        ///     Space complexity : O(1)
        /// </summary>
        /// <param name="nums">An array of integers.</param>
        /// <param name="val">An integer.</param>
        public int RemoveElement(int[] nums, int val)
        {
            if (nums.Length == 0) return 0;

            var length = nums.Length;

            for (var i = 0; i < length;)
                if (nums[i] == val) nums[i] = nums[--length];
                else i++;

            return length;
        }

        /// <summary>
        ///     Removes all the instances of a given value from an array.
        ///     Uses two pointers. Maintains order.
        ///     Two pointers can treverse upto 2n times.
        ///     Time complexity : O(n)
        ///     Space complexity : O(1)
        /// </summary>
        /// <param name="nums">An array of integers.</param>
        /// <param name="val">An integer.</param>
        public int RemoveElement2(int[] nums, int val)
        {
            if (nums.Length == 0) return 0;

            var length = 0;

            for (var i = 0; i < nums.Length; i++)
                if (nums[i] != val) nums[length++] = nums[i];

            return length;
        }
    }
}
